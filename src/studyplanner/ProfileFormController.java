/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @author Max Carmichael
 */
package studyplanner;

import java.awt.Button;
import java.awt.Dialog;
import java.awt.HeadlessException;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import static studyplanner.ProfileFormView.withinSevenDays;

/**
 *
 * @author 
 */
public class ProfileFormController {

    private final ProfileFormView profileView;
    private final ProfileFormModel model;
    private final StudyPlannerModel modelData;
    private final StudyPlannerView planView;
    private final Dialog dialog;
    private boolean fileUploaded = false;
    private String directory = System.getProperty("user.home");
    private String systemseparator = System.getProperty("file.separator");

    public ProfileFormController(ProfileFormView view, ProfileFormModel model,
            StudyPlannerModel modelData, StudyPlannerView planView, Dialog dialog) {
        this.profileView = view;
        this.model = model;
        this.modelData = modelData;
        this.planView = planView;
        this.dialog = dialog;
        view.addFileListener(new FileListener());
        view.saveData(new SaveListener());
    }

    public void initialiseProfileView() {
        int presentYear = new Date().getYear();
        int number = presentYear % 100;
        profileView.addComboBox("Spring" + (number - 1) + " - " + number);
        profileView.addComboBox("Summer" + (number - 1) + " - " + number);
        profileView.addComboBox("Winter" + (number - 1) + " - " + number);
        profileView.addComboBox("Fall" + (number - 1) + " - " + number);
    } 

    class FileListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser openFileDIalog = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("*.csv", "csv");

            openFileDIalog.setFileFilter(filter);
            int returnV = openFileDIalog.showOpenDialog(profileView);
            if (returnV == JFileChooser.APPROVE_OPTION) {
                File file = openFileDIalog.getSelectedFile();
                //This is where a real application would open the file.
                try {
                    Scanner toRead = new Scanner(file);
                    int row = 0;
                    int column = 0;

                    while (toRead.hasNextLine()) {
                        String command = toRead.nextLine();
                        String[] datas = command.split(",");

                        for (String data : datas) {
                            modelData.getModelMapper()[row][column] = data;
                            column++;
                        }
                        row++;
                        column = 0;
                    }
                    
                    mapDataObject(modelData.getModelMapper());
                    fileUploaded = true;
                    //  ***  read and parse each command string
                } catch (FileNotFoundException | ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
                parseDashBoard(file, planView.upcomingPanel());
            }
        }

    }

    private void parseDashBoard(File file, JPanel panel) {

        Set<String> keys = modelData.sevenDays().keySet();
        for (String key : keys) {
            Button button = new Button(key.split("\\*\\*\\*")[0]);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String presentDate = new SimpleDateFormat("MM/dd/yyy").format(new Date());
                    try {
                        if (key.split("\\*\\*\\*")[1].contains("StartDate")) {
                            Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                            Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) modelData.sevenDays().get(key).getStartDate());
                            long diff = Math.abs(d1.getTime() - d2.getTime());
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            JOptionPane.showMessageDialog(profileView,
                                    modelData.sevenDays().get(key).getModuleName() + "\n"
                                    + modelData.sevenDays().get(key).getStartDate() + "\n" + "" + diffDays + " day(s) left");

                        }

                        if (key.split("\\*\\*\\*")[1].contains("EndDate")) {
                            Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                            Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) modelData.sevenDays().get(key).getEndDate());
                            long diff = Math.abs(d1.getTime() - d2.getTime());
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            JOptionPane.showMessageDialog(profileView,
                                    modelData.sevenDays().get(key).getModuleName() + "\n"
                                    + modelData.sevenDays().get(key).getEndDate() + "\n" + "" + diffDays + " day(s) left");

                        }

                        if (key.split("\\*\\*\\*")[1].contains("WorkDueDate")) {
                            Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                            Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) modelData.sevenDays().get(key).getCourseWorkDueDate());
                            long diff = Math.abs(d1.getTime() - d2.getTime());
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            JOptionPane.showMessageDialog(profileView,
                                    modelData.sevenDays().get(key).getModuleName() + "\n"
                                    + modelData.sevenDays().get(key).getCourseWorkDueDate() + "\n" + "" + diffDays + " day(s) left");
                        }

                        if (key.split("\\*\\*\\*")[1].contains("ExamDate")) {
                            Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                            Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) modelData.sevenDays().get(key).getCourseWorkDueDate());
                            long diff = Math.abs(d1.getTime() - d2.getTime());
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            JOptionPane.showMessageDialog(profileView,
                                    modelData.sevenDays().get(key).getModuleName() + "\n"
                                    + modelData.sevenDays().get(key).getCourseWorkDueDate() + "\n" + "" + diffDays + " day(s) left");
                        }

                    } catch (HeadlessException | ParseException ex) {
                        ex.printStackTrace();
                    }
                }
            });

            panel.add(button);
            panel.add(new Label("10 days time"));
            panel.repaint();

        }
        Path from = Paths.get(file.getAbsolutePath());
        String fileName = profileView.getStudyName().getText();
        File createDirectory = new File(directory + systemseparator + "StudyPlanner" + systemseparator + fileName + ".csv");
        System.out.println("These is the directory " + createDirectory.getAbsolutePath());
        Path To = Paths.get(createDirectory.getAbsolutePath());
        CopyOption[] options = new CopyOption[]{
            StandardCopyOption.REPLACE_EXISTING,
            StandardCopyOption.COPY_ATTRIBUTES
        };
        try {
            Files.copy(from, To, options);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void mapDataObject(String[][] dataArray) {
        for (int c = 1; c < 4; c++) {
            SemesterCourse presentCourse = new SemesterCourse();
            presentCourse.setModuleName(dataArray[0][(c)]);
            presentCourse.setModuleCode(dataArray[1][c]);
            presentCourse.setStartDate(dataArray[2][c]);
            presentCourse.setEndDate(dataArray[3][c]);
            presentCourse.setCourseWorkCode(dataArray[4][c]);
            presentCourse.setCourseWorkDueDate(dataArray[5][c]);
            presentCourse.setExamCode(dataArray[6][c]);
            presentCourse.setExamDate(dataArray[7][c]);
            modelData.getMapData().put(dataArray[0][c], presentCourse);
        }

        Set<String> keys = modelData.getMapData().keySet();

        /*keys.forEach((key) -> {
            view.addComboItem(key);
        }); */
    }

    class SaveListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            boolean invalid = false;
            String fileName = profileView.getStudyName().getText();
            if (fileName == null || fileName.isEmpty()) {
                JOptionPane.showMessageDialog(profileView, "Please enter a valid name");
                invalid = true;
            }

            if (!invalid) { 
                if (!fileUploaded) {
                    JOptionPane.showMessageDialog(profileView, "Please upload a file");
                } else {
                    JOptionPane.showMessageDialog(profileView, "Data uploaded successfully");
                    dialog.dispose();
                }
            }

        }
    }

    private void saveFile(File file) {

    }

}
