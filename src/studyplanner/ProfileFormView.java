/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyplanner;

import java.awt.Button;
import java.awt.HeadlessException;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 *
 * @author Max Carmichael
 */
public class ProfileFormView extends javax.swing.JPanel {

    static HashMap<String, SemesterCourse> withinSevenDays = new HashMap<>();
    boolean fileUploaded = false;
    static File  originalFile;
    private static String DIRECTORY = System.getProperty("user.home");
    private static String SEPERATOR = System.getProperty("file.separator");
    private static String FILE_DIRECTORY = "favourites";
    static String fileName = "";
     static Date da = new Date();
     private static JTextField textField;
    

    /**
     * Creates new form ProfileForm
     */
    public ProfileFormView() {
        initComponents();
        semsester.removeAllItems();
        textField = studyName;
       
    }
    
    /*
    remove all combo box items
    @return void
    */
    public void removeItems() {
        semsester.removeAllItems();
    } 
    
    public void addComboBox(String item) {
        semsester.addItem(item);
    }
    
    /*
    add ActionListener to fileupload
    @param listener
    */
    public void addFileListener(ActionListener listener) {
        fileUpload.addActionListener(listener);
    }
    
     /*
    add ActionListener to button
    @param listener
    */
    public void saveData(ActionListener listener) {
        save.addActionListener(listener); 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        studyName = new javax.swing.JTextField();
        semsester = new javax.swing.JComboBox<>();
        fileUpload = new javax.swing.JButton();
        save = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        semsester.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        semsester.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                semsesterActionPerformed(evt);
            }
        });

        fileUpload.setText("File Upload");
        fileUpload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileUploadActionPerformed(evt);
            }
        });

        save.setText("Save");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });

        jLabel1.setText("Name:");

        jLabel2.setText("Semester:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(fileUpload, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(studyName, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(semsester, javax.swing.GroupLayout.Alignment.TRAILING, 0, 139, Short.MAX_VALUE)
                    .addComponent(save, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(90, 90, 90))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(studyName, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(semsester, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileUpload)
                    .addComponent(save))
                .addContainerGap(57, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void fileUploadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileUploadActionPerformed
      
    }//GEN-LAST:event_fileUploadActionPerformed
   
  
    public JTextField getStudyName() {
        return studyName;
    }


    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed
        // TODO add your handling code here:

        boolean invalid = false;

        String name = studyName.getText();
        
        if (name == null || name.isEmpty()) {
            JOptionPane.showMessageDialog(semsester, "Please enter a valid name");
            invalid = true;
        }

        if (!invalid) {
            if (!fileUploaded) {
                JOptionPane.showMessageDialog(semsester, "Please upload a file");
            } else {
                JOptionPane.showMessageDialog(semsester, "Data uploaded successfully");
                StudyPlannerView.dialogParent.dispose(); 
            }
        }


    }//GEN-LAST:event_saveActionPerformed

    private void semsesterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_semsesterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_semsesterActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton fileUpload;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton save;
    private javax.swing.JComboBox<String> semsester;
    private javax.swing.JTextField studyName;
    // End of variables declaration//GEN-END:variables
}
