package studyplanner;

import java.util.Date;

/**
 * The Event class, representing a coursework assignment or exam which 
 * possesses a deadline date 
 * 
 * @author Sam Griffiths
 */

public class Event 
{
    /**
     * The specific nature of an Event.
     */
    public enum EventType { COURSEWORK, EXAM };
    
    /**
     * Specific nature of this Event.
     */
    private EventType type;
    
    /**
     * Name of this Event.
     */
    private String name;
    
    /**
     * Deadline date of this Event.
     */
    private Date deadline;
    
    /**
     * Constructs a new Event with the given details.
     * @param name Name of the event
     * @param type Specific nature of the event
     * @param deadline Deadline date of the event
     */
    public Event(String name, EventType type, Date deadline) 
    {
        this.name = name;
        this.type = type;
        this.deadline = deadline;
    }
    
    /**
     * Gets the name of this Event.
     * @return Name of the event
     */
    public String name() { return name; }
    
    /**
     * Gets the EventType of this Event.
     * @return Specific nature of the event
     */
    public EventType type() { return type; }
    
    /**
     * Gets the deadline of this Event.
     * @return Deadline date of the event
     */
    public Date deadline() { return deadline; }
}
