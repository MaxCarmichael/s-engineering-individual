package studyplanner;

import java.util.ArrayList;

/**
 * The Module class, representing a named module in the semester which may 
 * possess Events (coursework assignments and exams).
 * 
 * @author Sam Griffiths
 */

public class Module 
{
    /**
     * Code of this Module.
     */
    private String code;
    
    /**
     * Name of this Module.
     */
    private String name;
    
    /**
     * Collection of Events associated with this Module.
     */
    private ArrayList<Event> events;
    
    /**
     * Constructs a new Module with the given Events.
     * @param code Module code
     * @param name Module name
     * @param events Events associated with this Module
     */
    public Module(String code, String name, ArrayList<Event> events)
    {
        this.code = code;
        this.name = name;
        this.events = events;
    }
    
    /**
     * Constructs a new empty Module.
     * @param code Module code
     * @param name Module name
     */
    public Module(String code, String name)
    {
        this(code, name, new ArrayList<>());
    }
    
    /**
     * Gets the code of this Module.
     * @return Module code
     */
    public String code() { return code; }
    
    /**
     * Gets the name of this Module.
     * @return Module name
     */
    public String name() { return name; }
    
    /**
     * Gets the Events associated with this Module.
     * @return Events associated with this Module
     */
    public ArrayList<Event> events() { return events; }
    
    /**
     * Associates the given Event with this Module.
     * @param event Event to associate
     */
    public void addEvent(Event event) { events.add(event); }
}
