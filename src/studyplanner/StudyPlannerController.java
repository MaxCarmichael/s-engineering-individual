package studyplanner;

import java.awt.Button;
import java.awt.HeadlessException;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.jdatepicker.impl.JDatePickerImpl;
import static studyplanner.StudyPlannerView.dialogParent;

/**
 * The StudyPlannerController class, containing methods for processing user
 * input.
 *
 * As a Controller, it contains references to both the View and the Model, in
 * order that it may instruct the former to change its display and the latter to
 * change its state.
 *
 * @author Max Carmichael
 */
public class StudyPlannerController {

    private String directory = System.getProperty("user.home");
    private String systemseparator = System.getProperty("file.separator");
    private String presentList = "";
    
    HashMap<String, SemesterCourse> notSureYet = new HashMap();
    HashMap<String, Module> events = new HashMap<>();
    JComboBox<String> comboBox;

    /**
     * Reference held to the View.
     */
    private final StudyPlannerView view;

    /**
     * Reference held to the Model.
     */
    private final StudyPlannerModel model;

    /**
     * Constructs a Controller with references held to the given View and Model.
     *
     * @param view StudyPlannnerView to reference
     * @param model StudyPlannerModel to reference
     */
    public StudyPlannerController(StudyPlannerView view, StudyPlannerModel model) {
        this.view = view;
        this.model = model;
        this.comboBox = view.getCombo();
        view.addCalenderListener(new CalendarListener());
        view.addFileListener(new FileListener());
        view.addCreateStudyPlanListener(new createStudyplan());
        addComboListener();
    }

    public void createDirectory() {
        File createDirectory = new File(directory + systemseparator + "StudyPlanner");
        if (!createDirectory.isDirectory()) {
            createDirectory.mkdir();
        }
    }
    
    /*
     Query all previous data stored on the disk and populate calendar
     @param panel the panel where data is displayed
     @return void
    */
    public void initialiseFiles(JPanel panel) throws IOException {
        File directory = new File(this.directory + systemseparator + "StudyPlanner");
        File[] directoryFile = directory.listFiles();
        for (File file : directoryFile) {
            Scanner toRead = new Scanner(file);
            int row = 0;
            int column = 0;

            while (toRead.hasNextLine()) {
                String command = toRead.nextLine();
                String[] datas = command.split(",");

                for (String data : datas) {
                    model.getModelMapper()[row][column] = data;
                    column++;
                }
                row++;
                column = 0;
            }
            mapSevenDays(model.getModelMapper());
        }
        Set<String> keys = model.sevenDays().keySet();
        for (String key : keys) {
            Button upcomingEvents = new Button(key.split("\\*\\*\\*")[0]);
            upcomingEvents.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String presentDate = new SimpleDateFormat("MM/dd/yyy").format(new Date());

                    try {
                        if (key.split("\\*\\*\\*")[1].contains("StartDate")) {
                            Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                            Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) model.sevenDays().get(key).getStartDate());
                            long diff = Math.abs(d1.getTime() - d2.getTime());
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            JOptionPane.showMessageDialog(Dashboard.dialogParent,
                                    model.sevenDays().get(key).getModuleName() + "\n"
                                    + model.sevenDays().get(key).getStartDate() + "\n" + "" + diffDays + " day(s) left");
                        } 

                        if (key.split("\\*\\*\\*")[1].contains("EndDate")) {
                            Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                            Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) model.sevenDays().get(key).getEndDate());
                            long diff = Math.abs(d1.getTime() - d2.getTime());
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            JOptionPane.showMessageDialog(Dashboard.dialogParent,
                                    model.sevenDays().get(key).getModuleName() + "\n"
                                    + model.sevenDays().get(key).getEndDate() + "\n" + "" + diffDays + " day(s) left");

                        }

                        if (key.split("\\*\\*\\*")[1].contains("WorkDueDate")) {
                            Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                            Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) model.sevenDays().get(key).getCourseWorkDueDate());
                            long diff = Math.abs(d1.getTime() - d2.getTime());
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            JOptionPane.showMessageDialog(Dashboard.dialogParent,
                                    model.sevenDays().get(key).getModuleName() + "\n"
                                    + model.sevenDays().get(key).getCourseWorkDueDate() + "\n" + "" + diffDays + " day(s) left");
                        }

                        if (key.split("\\*\\*\\*")[1].contains("ExamDate")) {
                            Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                            Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) model.sevenDays().get(key).getCourseWorkDueDate());
                            long diff = Math.abs(d1.getTime() - d2.getTime());
                            long diffDays = diff / (24 * 60 * 60 * 1000);
                            JOptionPane.showMessageDialog(Dashboard.dialogParent,
                                    model.sevenDays().get(key).getModuleName() + "\n"
                                    + model.sevenDays().get(key).getCourseWorkDueDate() + "\n" + "" + diffDays + " day(s) left");
                        }  

                    } catch (HeadlessException | ParseException ex) {
                        ex.printStackTrace();
                    }
                }
            });
            
                panel.add(upcomingEvents);
                panel.add(new Label("Less than 7 days left")); 
                panel.repaint();
        }
         recoupleList(); 
    }
    /*
    
    Display data between 7 days of the exam
    @param dataArray data object to map data
    @return void
    */
    private void mapSevenDays(String[][] dataArray) {
  
        String presentDate = new SimpleDateFormat("MM/dd/yyy").format(new Date());

        for (int c = 1; c < 4; c++) {
            try {
               
                SemesterCourse presentCourse = new SemesterCourse();
                
                Date d1 = new SimpleDateFormat("MM/dd/yyy").parse((String) presentDate);
                Date d2 = new SimpleDateFormat("MM/dd/yyy").parse((String) dataArray[2][c]);
                Date d3 = new SimpleDateFormat("MM/dd/yyy").parse((String) dataArray[3][c]);
                Date d4 = new SimpleDateFormat("MM/dd/yyy").parse((String) dataArray[5][c]);
                Date d5 = new SimpleDateFormat("MM/dd/yyy").parse((String) dataArray[7][c]);
                  Module module = new Module(dataArray[1][c], dataArray[0][(c)]);
            

                long diff = Math.abs(d1.getTime() - d2.getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);
               
                if (diffDays >= 0 && diffDays <= 7) {
                  
                    Event event = new Event("StartDate", Event.EventType.COURSEWORK, new Date(dataArray[2][c])); 
                    module.events().add(event);
                    
                    
                    model.sevenDays().put(dataArray[0][(c)] + "***StartDate", presentCourse);
                }

                diff = Math.abs(d1.getTime() - d3.getTime());
                diffDays = diff / (24 * 60 * 60 * 1000);
               
                if (diffDays >= 0 && diffDays <= 7) {
                    Event event = new Event("EndDate", Event.EventType.COURSEWORK, new Date(dataArray[3][c])); 
                    module.events().add(event);
                   model.sevenDays().put(dataArray[0][(c)] + "***EndDate", presentCourse);
                }

                diff = Math.abs(d1.getTime() - d4.getTime());
                diffDays = diff / (24 * 60 * 60 * 1000);
                
                if (diffDays >= 0 && diffDays <= 7) {
                     Event event = new Event("WorkDueDate", Event.EventType.COURSEWORK, new Date(dataArray[5][c])); 
                    module.events().add(event);
                    model.sevenDays().put(dataArray[0][(c)] + "***WorkDueDate", presentCourse);
                }

                diff = Math.abs(d1.getTime() - d5.getTime());
                diffDays = diff / (24 * 60 * 60 * 1000);
            

                if (diffDays >= 0 && diffDays <= 7) {
                        Event event = new Event("ExamDate", Event.EventType.EXAM, new Date(dataArray[7][c])); 
                    module.events().add(event);
                    model.sevenDays().put(dataArray[0][(c)] + "***ExamDate", presentCourse);
                }
                
                presentCourse.setModuleName(dataArray[0][(c)]);
                presentCourse.setModuleCode(dataArray[1][c]);
                presentCourse.setStartDate(dataArray[2][c]);
                presentCourse.setEndDate(dataArray[3][c]);
                presentCourse.setCourseWorkCode(dataArray[4][c]);
                presentCourse.setCourseWorkDueDate(dataArray[5][c]);
                presentCourse.setExamCode(dataArray[6][c]);
                presentCourse.setExamDate(dataArray[7][c]);
                events.put(module.name(), module);
                notSureYet.put(dataArray[0][c], presentCourse);
                model.getMapData().put(dataArray[0][c], presentCourse);
              

            } catch (ParseException e) {
                e.printStackTrace(); 
            }
            
            

        } 
       
    }
    
    /*
     Add combo listener to wait for changes on combo box
    */
    private void addComboListener() {
        
        this.comboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                  String item = (String)e.getItem();
                    presentList = item;
                }
            }
        });   
    }
    /*
     Implement calendar change date listener
     to query data object and show all dates on the calendar table
    */
    class CalendarListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
  
          
            Object[][] calendarData = model.getCalendarView();
            String time = "";
            Set<String> keys = model.getMapData().keySet();
           
            for (String key : keys) {   

                if(!presentList.contains("All") && !key.contains(presentList)) {
                    continue;
                }
                
               
                SemesterCourse semCourse = model.getMapData().get(key);
                String dateDay = splitTextField(view.datePicker, 0);
               
                time = splitTextField(view.datePicker, 1);
                int day = 0;

                switch (dateDay.toLowerCase()) {
                    case "mon":
                        day = 1;
                        break;
                    case "tue":
                        day = 2;
                        break;
                    case "wed":
                        day = 3;
                        break;
                    case "thur":
                        day = 4;
                        break;
                    case "fri":
                        day = 5;
                        break;
                    case "sat":
                        day = 6;
                        break;
                    case "sun":
                        day = 7;
                    default:
                        day = 1;

                }
                
              
                if (time.contains(semCourse.getExamDate())) {
                    System.out.println("The date is "+ semCourse.getExamDate() + "within ");
                    JLabel label = new JLabel("<html> " + key + ""
                            + " <br/> <span style='color: blue'> "
                            + semCourse.getModuleCode() + "</span> <br/> <span style='color: red'>"
                            + "Exam Date: </span>" + " </html>");

                    if (calendarData[day][2] == null) {
                        calendarData[day][2] = label.getText();
                    } else {
                        calendarData[day][2] += label.getText();
                    }

                }

                if (time.contains(semCourse.getStartDate())) {
                         System.out.println("The date is "+ semCourse.getStartDate()+ "within ");
                    JLabel label = new JLabel("<html> " + key
                            + " <br/> <span style='color: blue'> "
                            + semCourse.getModuleCode() + "</span> <br/> <span style='color: red'>"
                            + "Start Date: </span>" + " </html>");

                    if (calendarData[day][4] == null) {
                        calendarData[day][4] = label.getText();
                    } else {
                        calendarData[day][4] += label.getText();
                    }
                }

                if (time.contains(semCourse.getCourseWorkDueDate())) {
                         System.out.println("The date is "+ semCourse.getCourseWorkCode() + "within ");
                    JLabel label = new JLabel("<html> " + key + " <br/> <span style='color: blue'> " + semCourse.getModuleCode() + "</span> <br/> <span style='color: yellow'>" + "Course work due: </span>" + " </html>");

                    if (calendarData[day][6] == null) {
                        calendarData[day][6] = label.getText();
                    } else {
                        calendarData[day][6] += label.getText();
                    }
                }

                if (time.contains(semCourse.getEndDate())) {
                         System.out.println("The date is "+ semCourse.getEndDate() + "within ");
                    JLabel label = new JLabel("<html> " + key + " <br/> <span style='color: blue'> " + semCourse.getModuleCode() + "</span> <br/> <span style='color: green'>" + "End Date: </span>" + " </html>");
                    if (calendarData[day][7] == null) {
                        calendarData[day][7] = label.getText();
                    } else {
                        calendarData[day][7] += label.getText();
                    }
                }

            } 
            view.getTable().setModel(new javax.swing.table.DefaultTableModel(calendarData,
                    new String[]{"Time", "Monday", "Tuesday", "Wednesday", "Thursday",
                        "Friday", "Saturday", "Sunday"}));
            
            if(!time.isEmpty()) 
            view.datePicker.getJFormattedTextField().setText(time);

        }
    }

    public String splitTextField(JDatePickerImpl datePicker, int index) {
        return datePicker.getJFormattedTextField().getText().split(",")[index];
    }

    /*
    Display profile form dialog for filling in study plan
    */
    class createStudyplan implements ActionListener { 

        @Override
        public void actionPerformed(ActionEvent e) {
            JDialog dialog = new JDialog(view);
            dialog.setBounds(100, 100, 300, 200);
            ProfileFormView form = new ProfileFormView();
            ProfileFormModel modelProfile = new ProfileFormModel();
            ProfileFormController controller = new ProfileFormController(form, modelProfile, model, view, dialog);
            controller.initialiseProfileView();
            dialog.add(form);
            dialog.show();
        } 
    }
    
    /*
     Read file from user and parse to dataobject
    */
    class FileListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser openFileDIalog = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("*.csv", "csv");

            openFileDIalog.setFileFilter(filter);
            int returnV = openFileDIalog.showOpenDialog(view);
            if (returnV == JFileChooser.APPROVE_OPTION) {
                File file = openFileDIalog.getSelectedFile();
                //This is where a real application would open the file.
                try {
                    Scanner toRead = new Scanner(file);
                    int row = 0;
                    int column = 0;

                    while (toRead.hasNextLine()) {
                        String command = toRead.nextLine();
                        String[] datas = command.split(",");
        

                        for (String data : datas) {
                         
                            model.getModelMapper()[row][column] = data;
                            column++;
                        }
                        row++;
                        column = 0;
                    }

                    mapDataObject(model.getModelMapper());
                    //  ***  read and parse each command string
                } catch (FileNotFoundException | ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }
    
    private void recoupleList(){
       
      Set<String> keys = model.getMapData().keySet();
        

        keys.forEach((key) -> {
            view.addComboItem(key);
        });
        
    }

    private void mapDataObject(String[][] dataArray) {
        for (int c = 1; c < 4; c++) {
            SemesterCourse presentCourse = new SemesterCourse();
            presentCourse.setModuleName(dataArray[0][(c)]);
            presentCourse.setModuleCode(dataArray[1][c]);
            presentCourse.setStartDate(dataArray[2][c]);
            presentCourse.setEndDate(dataArray[3][c]);
            presentCourse.setCourseWorkCode(dataArray[4][c]);
            presentCourse.setCourseWorkDueDate(dataArray[5][c]);
            presentCourse.setExamCode(dataArray[6][c]);
            presentCourse.setExamDate(dataArray[7][c]);
            model.getMapData().put(dataArray[0][c], presentCourse);
        }

        Set<String> keys = model.getMapData().keySet();

        keys.forEach((key) -> {
            view.addComboItem(key);
        });
        
       
        
        
              
    }
}
