package studyplanner;

import java.util.Observable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The StudyPlannerModel class, representing the data and data logic of the
 * application.
 *
 * As a Model, it is unaware of the View and Controller. However, it is an
 * Observable object which can notify its Observers of changes and allow data to
 * be pulled through its public methods.
 *
 * @author Max Carmichael
 */
public class StudyPlannerModel extends Observable {

    private String[][] dataStructure = new String[8][4];
    private HashMap<String, SemesterCourse> data = new HashMap<>();
    private Object[][] calendarView;
    private HashMap<String, SemesterCourse> withinSevenDays = new HashMap();
    private HashMap<String, Module> events = new HashMap<>();
    /**
     * Collection of Modules, which in turn possess Events.
     */
    private ArrayList<Module> modules;

    /**
     * Notifies Observers of changes to data; to be invoked when changes are
     * made which immediately affect the current View.
     */
    private void update() {
        setChanged();
        notifyObservers();
    } 

    public String[][] getModelMapper() {
        return dataStructure;
    }

    public HashMap<String, SemesterCourse> getMapData() {
        return data;
    }
    
    public HashMap<String, SemesterCourse> sevenDays() {
        return withinSevenDays;
    }

    public Object[][] getCalendarView() {
        return calendarView = new Object[][]{
            {"9", null, null, null, null, null, null, null},
            {"", null, null, null, null, null, null, null},
            {"10", null, null, null, null, null, null, null},
            {"", null, null, null, null, null, null, null},
            {"11", null, null, null, null, null, null, null},
            {"", null, null, null, null, null, null, null},
            {"12", null, null, null, null, null, null, null},
            {"", null, null, null, null, null, null, null},
            {"1", null, null, null, null, null, null, null},
            {"", null, null, null, null, null, null, null},
            {"2", null, null, null, null, null, null, null},
            {"", null, null, null, null, null, null, null},
            {"3", null, null, null, null, null, null, null},
            {"", null, null, null, null, null, null, null},
            {"4", null, null, null, null, null, null, null},
            {null, null, null, null, null, null, null, null},
            {null, null, null, null, null, null, null, null}
        };
    }
}
