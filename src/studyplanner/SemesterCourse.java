/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyplanner;

/**
 *
 * @author Max Carmichael
 */
public class SemesterCourse {
    
    private String moduleName;
    private String moduleCode;
    private String startDate;
    private String endDate;
    private String courseWorkCode;
    private String courseWorkDueDate;
    private String examCode;
    private String examDate;
    
    /*
      get the name of the module
      @return moduleName
    */
    public String getModuleName() {
        return moduleName;
    }
    
    /*
      set the name of the module
      @param moduleName the module
      @return void
    */
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
    
    /*
      get the code of the module
      @return moduleCode
    */
    public String getModuleCode() {
        return moduleCode;
    }

    /*
      set the module code
      @param moduleCode the modulecode
      @return void
    */
    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }
    
    /*
      get start date
      @return start Date
    */
    public String getStartDate() {
        return startDate;
    }
    
    /*
      set the module code
      @param startDate the startDate
      @return void
    */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    
    /*
     gets end date
     @return endDate
    */
    public String getEndDate() {
        return endDate;
    }
    
    /*
      set the end date
      @param endDate the endDate
      @return void
    */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCourseWorkCode() {
        return courseWorkCode;
    }

    public void setCourseWorkCode(String courseWorkCode) {
        this.courseWorkCode = courseWorkCode;
    }

    public String getCourseWorkDueDate() {
        return courseWorkDueDate;
    }

    public void setCourseWorkDueDate(String courseWorkDueDate) {
        this.courseWorkDueDate = courseWorkDueDate;
    }
    
    /*
      get Exam Code
      @return examCode
    */
    public String getExamCode() {
        return examCode;
    }
    
    /*
      set the exam code
      @param examCode the examCode
      @return void
    */
    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }
    
    /*
      get Exam date
      @return examDate
    */
    public String getExamDate() {
        return examDate;
    }
    
    /*
      set the exam date
      @param examDate the examDate
      @return void
    */
    public void setExamDate(String examDate) {
        this.examDate = examDate;
    }
    
    
}
